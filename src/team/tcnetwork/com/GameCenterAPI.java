package team.tcnetwork.com;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.bukkit.plugin.java.JavaPlugin;

public class GameCenterAPI extends JavaPlugin {
	
	Logger log = Logger.getLogger("Minecraft");
	
	public void onEnable() {
		log.log(Level.INFO, "GameCenter API has been enabled!");
	}
	
	public void onDisable() {
		log.log(Level.INFO, "GameCenter API has been disabled!");
	}
	
}